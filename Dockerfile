ARG IMAGE_TAG=python:3.8-slim

FROM $IMAGE_TAG

RUN mkdir /app /work /data

COPY requirements.txt /tmp/requirements.txt
RUN pip install -r /tmp/requirements.txt

COPY server.py /app/server.py

WORKDIR /work

CMD ["python", "/app/server.py"]
