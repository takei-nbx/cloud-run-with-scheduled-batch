import json
import os
import slackweb
from bottle import abort, request, route, run, template
from google.cloud import pubsub_v1


def verify_token(func):
    def _verify_token(*arg, **kwargs):
        svr_token = os.environ.get('PUBSUB_TOKEN', None)
        if not svr_token:
            abort(500, "Server error")
        req_token = request.query.get('token', None)
        if not req_token:
            abort(400, "Invalid request")
        if req_token != svr_token:
            abort(400, "Invalid token")
        return func(*arg, **kwargs)
    return _verify_token


@route('/hello/<name>')
def index(name):
    return template('<b>Hello {{name}}</b>!', name=name)


@route('/_hooks/pubsub/entry', method='POST')
@verify_token
def _entry():
    publisher = pubsub_v1.PublisherClient()
    topic_name = 'projects/{project_id}/topics/{topic}'.format(
        project_id=os.getenv('GOOGLE_CLOUD_PROJECT'),
        topic='postslack',  # Set this to something appropriate.
    )
    for neta in ['Tako', 'Ika']:
        publisher.publish(topic_name, b'My first message!', neta=neta)
    return 'OK'


@route('/_hooks/pubsub/postslack', method='POST')
@verify_token
def _postslack():
    webhook_url = os.environ.get('WEBHOOK_URL', None)
    if not webhook_url:
        abort(500)
    postdata = json.load(request.body)
    neta = postdata['message']['attributes']['neta']
    slack = slackweb.Slack(url=webhook_url)
    slack.notify(text=f"{neta} is a sushi", username="sushi-bot", icon_emoji=":sushi:")
    return 'OK'


if __name__ == '__main__':
    port = os.environ.get('PORT', 8080)
    run(host='0.0.0.0', port=port)
